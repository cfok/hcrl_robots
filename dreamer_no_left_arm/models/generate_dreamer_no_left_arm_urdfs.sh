#!/bin/bash

echo Creating directory \'urdf\'
mkdir -p dreamer_no_left_arm/urdf

echo Generating dreamer_no_left_arm/urdf/dreamer_no_left_arm_with_gazebo_plugins.urdf
rosrun xacro xacro.py dreamer_no_left_arm/xacro/dreamer_no_left_arm_with_gazebo_plugins.xacro -o dreamer_no_left_arm/urdf/dreamer_no_left_arm_with_gazebo_plugins.urdf

echo Generating dreamer_no_left_arm/urdf/dreamer_no_left_arm_rapid.urdf
rosrun xacro xacro.py dreamer_no_left_arm/xacro/dreamer_no_left_arm_rapid.xacro -o dreamer_no_left_arm/urdf/dreamer_no_left_arm_rapid.urdf

echo Done!