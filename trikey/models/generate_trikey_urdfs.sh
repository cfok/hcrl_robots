#!/bin/bash

echo Creating directory \'urdf\'
mkdir -p trikey/urdf

echo Generating trikey/urdf/trikey_rapid.urdf
rosrun xacro xacro.py trikey/xacro/trikey_rapid.xacro -o trikey/urdf/trikey_rapid.urdf

echo Generating trikey/urdf/trikey_with_gazebo_plugins.urdf
rosrun xacro xacro.py trikey/xacro/trikey_with_gazebo_plugins.xacro -o trikey/urdf/trikey_with_gazebo_plugins.urdf

echo Done!