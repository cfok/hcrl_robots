#!/bin/bash

rostopic pub -1 /right_hand_goal_position std_msgs/Float64MultiArray '{layout: {dim: [{label: "x", size: 3, stride: 3}], data_offset: 0}, data: [0.0, 1.0, 0.6, 0.6, 0.6]}'

