#!/bin/bash

echo Creating directory \'urdf\'
mkdir -p dreamer_testbed/urdf

echo Generating dreamer_testbed/urdf/dreamer_testbed_with_gazebo_plugins.urdf
rosrun xacro xacro.py dreamer_testbed/xacro/dreamer_testbed_with_gazebo_plugins.xacro -o dreamer_testbed/urdf/dreamer_testbed_with_gazebo_plugins.urdf

echo Done!