#!/bin/bash

echo Creating directory \'urdf\'
mkdir -p trikey_pinned/urdf

echo Generating trikey_pinned/urdf/trikey_pinned_with_gazebo_plugins.urdf
rosrun xacro xacro.py trikey_pinned/xacro/trikey_pinned_with_gazebo_plugins.xacro -o trikey_pinned/urdf/trikey_pinned_with_gazebo_plugins.urdf

echo Generating trikey_pinned/urdf/trikey_pinned_rapid.urdf
rosrun xacro xacro.py trikey_pinned/xacro/trikey_pinned_rapid.xacro -o trikey_pinned/urdf/trikey_pinned_rapid.urdf

echo Done!